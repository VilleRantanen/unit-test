const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests\n", () => {
    before(() => {
        // Initilization
        // Create objects... etc...
        console.log("Initializing tests\n");
    });

    // Tests
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can subtract 4 from 6", () => {
        expect(mylib.subtract(6, 4)).equal(2, "6 - 4 is not 2, for some reason?");
    });
    it("Can divide 8 by 2", () => {
        expect(mylib.divide(8, 2)).equal(4, "8 / 2 is not 4, for some reason?");
    });
    it("Can multiply 3 by 4", () => {
        expect(mylib.multiply(3, 4)).equal(12, "3 * 4 is not 12, for some reason?");
    });
    it("Can catch divide by zero", () => {
        expect(() => mylib.divide(8, 0)).to.throw("Cannot divide by zero");
    });

    after(() => {
        // Cleanup
        // For example: shutdownd the Express server.
        console.log("\nTesting complited!");
    });
});