/** Basic arithmetic operations */
const mylib = {
    
    /** Multiline arrow function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return  a - b;
    },
    /** Singleline arrow function, that throwsa an error if the divisor is 0. */
    divide: (divident, divisor) => divisor === 0 ? (() => { throw new Error("Cannot divide by zero"); })() : divident / divisor,
    /** Regular function */
    multiply: function (a, b) {
        return a * b;
    }

};

module.exports = mylib;